# Devops-project

Devops project est un projet devops effectué dans le cadre du cours de Devops.

## Cloner le projet
```    
    git clone https://gitlab.com/BOUABDELLAHM/devops-project.git 
```

## Installation et lancement du projet
```
cd devops-project
npm install
node app.js
```

Ensuite se rendre sur localhost:3000 affin d'afficher l'app, si "Hello World" s'affiche, alors l'application fonctionne