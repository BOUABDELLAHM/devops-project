#!/usr/bin/env bats

@test "Check Home page" {
  curl -sfq my-app:3000/ | fgrep Hello
}