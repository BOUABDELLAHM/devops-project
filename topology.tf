data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "my_internet_gateway" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name  = "internet_gateway_devops_project"
    Group = "ESGI_DEVOPS_GROUP_3"
  }
}

resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_internet_gateway.id
  }

  tags = {
    Name  = "route_table_devops_project"
    Group = "ESGI_DEVOPS_GROUP_3"
  }
}

resource "aws_subnet" "my_subnet" {
  availability_zone = "us-east-1a"
  cidr_block        = "10.0.8.0/24"
  vpc_id            = aws_vpc.my_vpc.id
  tags = {
    Name  = "subnet_devops_project"
    Group = "ESGI_DEVOPS_GROUP_3"
  }
}

resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}

resource "aws_security_group" "my_security_group" {
  name        = "Allow SSH"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.my_vpc.id
  tags = {
    Name  = "security_group_devops_project"
    Group = "ESGI_DEVOPS_GROUP_3"
  }
}

resource "aws_security_group" "my_security_group_for_egress" {
  name_prefix = "security_group_devops_project"
  vpc_id      = aws_vpc.my_vpc.id
  tags = {
    Name  = "security_group_devops_project"
    Group = "ESGI_DEVOPS_GROUP_3"
  }
}

resource "aws_security_group_rule" "my_security_group_rule_in_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_out_ssh" {
  type              = "egress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group_for_egress.id
}

resource "aws_security_group_rule" "my_security_group_rule_out_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group_for_egress.id
}



resource "aws_security_group_rule" "my_security_group_rule_in_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group_for_egress.id
}

resource "aws_security_group_rule" "my_security_group_rule_in_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group_for_egress.id
}

resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCoCFhiN2XylFCyicyVkN5wxYnsx8tc21I2buAH214Qjz/203nz1xEMMOAhNM4ungdI9j4hHK07TPwKisInaVi24HUZYYcBIWB+M73HLuZMbyZRGMJLCcUreJbXd6SDNS9OoLGqOju0LwiEbQxlXzD/ATKvhYPCgmJ51RHVlkjD7QzKRFgLWegwlLDE1iQkzCe50DSZ1kfHPbC2wYjgpVW+NFmRWghKuU+PMu2CdhXP10ZrPL4sR9U0cdkGjGeI7HHq9zYN6KhEf1HCZw7K6ZKKpF4N26dLCbbbmr6dk7uFYUoYpYL/7VyxehVXf3+aUEOgb+wuP5JENvaGugm+05Ml8i8P9gUwnkRFu0mMnmXskO7EvEQ6XrsI8AYAVCoA4/+npQevZ9BncMo5v5C6B+xxowaWWgRzc6JbDCjbajyiuYd9cSNFWqMiF/qR3vlXXMrOzHOp63mhCrE62zlSES+d4Y8DPfXgTaQqvYjlB2OH0ueNFAwxvVsWVw6HaFyPlg0= yassinebousaidi@MacBook-Pro-de-Yassine.local"
}

resource "aws_instance" "web" {
  ami                         = data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.my_subnet.id
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.my_security_group.id, aws_security_group.my_security_group_for_egress.id]
  associate_public_ip_address = true
  key_name                    = aws_key_pair.admin.key_name

  tags = {
    Name  = "instance_devops_project"
    Group = "ESGI_DEVOPS_GROUP_3"
  }
}